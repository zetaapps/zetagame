package zetaapps.flickr.models.flickr.errors;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.support.v4.util.Pair;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.annotation.ParametersAreNonnullByDefault;

import zetaapps.flickr.models.common.errors.BaseException;

@ParametersAreNonnullByDefault
public class FlickrException extends BaseException {

    public FlickrException(@Nullable Pair<String, String> errorCodeAndParam) {
        super(errorCodeAndParam);
    }

    @Retention(RetentionPolicy.SOURCE)
    @StringDef({PrzError.NO_IMAGES_FOUND, PrzError.UNKNOWN})
    public @interface PrzError {
        String NO_IMAGES_FOUND = "no images found";
        String UNKNOWN = "UNKNOWN";
    }

    @PrzError
    @NonNull
    @Override
    public String getErrorCode() {
        if (errorCode == null) {
            return PrzError.UNKNOWN;
        }

        switch (errorCode) {
            case PrzError.NO_IMAGES_FOUND:
                return PrzError.NO_IMAGES_FOUND;
            default:
                return PrzError.UNKNOWN;
        }
    }
}
