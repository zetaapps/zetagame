package zetaapps.flickr.modules;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import zetaapps.flickr.api.FlickrApi;
import zetaapps.flickr.api.response.FlickrImageResponse;
import zetaapps.flickr.managers.params.flickr.FlickrImageManager;
import zetaapps.flickr.models.common.ITransformer;
import zetaapps.flickr.models.flickr.FlickrImageModel;
import zetaapps.flickr.models.transformers.FlickrModelTransformer;
import zetaapps.flickr.qualifiers.RetrofitFlickr;

@Module
public class FlickrImageModule {

    @Provides
    FlickrApi providesFlickrApi(@RetrofitFlickr Retrofit retrofit) {
        return retrofit.create(FlickrApi.class);
    }

    @Provides
    ITransformer<FlickrImageResponse, FlickrImageModel> providesFlickrModelTransformer() {
        return new FlickrModelTransformer();
    }

    @Provides
    FlickrImageManager providesFlickrImageManager(FlickrApi flickrApi, ITransformer<FlickrImageResponse, FlickrImageModel> transformer) {
        return new FlickrImageManager(flickrApi, transformer);
    }
}
