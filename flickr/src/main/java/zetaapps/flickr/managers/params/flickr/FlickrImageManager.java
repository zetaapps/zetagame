package zetaapps.flickr.managers.params.flickr;

import com.fernandocejas.frodo.annotation.RxLogObservable;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;

import rx.Observable;
import zetaapps.flickr.api.FlickrApi;
import zetaapps.flickr.api.response.FlickrImageResponse;
import zetaapps.flickr.models.common.ITransformer;
import zetaapps.flickr.models.common.Managers;
import zetaapps.flickr.models.common.OneOf;
import zetaapps.flickr.models.flickr.FlickrImageModel;
import zetaapps.flickr.models.flickr.errors.FlickrException;

@ParametersAreNonnullByDefault
public class FlickrImageManager {

    private final FlickrApi mFlickrApi;
    private final ITransformer<FlickrImageResponse, FlickrImageModel> mFlickerTransfomer;

    @Inject
    public FlickrImageManager(FlickrApi flickrApi, ITransformer<FlickrImageResponse, FlickrImageModel> transformer) {
        mFlickrApi = flickrApi;
        mFlickerTransfomer = transformer;
    }

    @RxLogObservable
    public Observable<OneOf<FlickrImageModel, FlickrException>> getFlickrImages() {
        return mFlickrApi.getImagesFromFlickr("json", "140440909@N04", 1)
                .map(response -> Managers.buildOneOf(
                        response,
                        FlickrException::new,
                        mFlickerTransfomer::transform));
    }

}
