package zetaapps.flickr.managers.params.basicauth;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class BasicAuthParams implements Parcelable {

    public static Builder create(String userName, String password) {
        return new AutoValue_BasicAuthParams.Builder()
                .setUserName(userName)
                .setPassword(password);
    }

    public abstract String getUserName();

    public abstract String getPassword();

    @AutoValue.Builder
    public static abstract class Builder {

        public abstract Builder setUserName(String userName);

        public abstract Builder setPassword(String password);

        public abstract BasicAuthParams build();
    }

}
