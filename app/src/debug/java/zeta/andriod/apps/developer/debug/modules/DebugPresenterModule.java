package zeta.andriod.apps.developer.debug.modules;

import com.github.pedrovgs.lynx.LynxConfig;

import dagger.Module;
import dagger.Provides;
import zeta.andriod.apps.dagger.FragmentScope;
import zeta.andriod.apps.developer.debug.presenter.DebugPresenter;
import zeta.andriod.apps.rx.interfaces.RxSchedulerProvider;
import zeta.andriod.apps.sharedPref.DebugSharedPreferences;

@Module
@FragmentScope
public class DebugPresenterModule {

    @Provides
    DebugPresenter providesHomePresenter(RxSchedulerProvider schedulerProvider,
                                         DebugSharedPreferences sharedPreferences,
                                         LynxConfig lynxConfig) {
        return new DebugPresenter(schedulerProvider, sharedPreferences, lynxConfig);
    }

}
