package zeta.andriod.apps.modules;

import java.util.List;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.logging.HttpLoggingInterceptor;
import timber.log.Timber;
import zeta.andriod.apps.dagger.OkHttpInterceptors;
import zeta.andriod.apps.dagger.OkHttpNetworkInterceptors;
import zeta.andriod.apps.network.BasicAuthInterceptor;
import zeta.andriod.apps.providers.interfaces.SharedPrefProvider;
import zeta.andriod.apps.sharedPref.DebugSharedPreferences;

import static java.util.Collections.singletonList;

@Module
@ParametersAreNonnullByDefault
public class OkHttpInterceptorsModule {

    @Provides
    @Singleton
    public HttpLoggingInterceptor provideHttpLoggingInterceptor(DebugSharedPreferences sharedPreferences) {
        final HttpLoggingInterceptor logging = new HttpLoggingInterceptor(message -> Timber.d(message));
        logging.setLevel(sharedPreferences.getHttpLoggingLevel());
        return logging;
    }

    @Provides
    @Singleton
    @OkHttpInterceptors
    public List<Interceptor> provideOkHttpInterceptors(HttpLoggingInterceptor httpLoggingInterceptor) {
        return singletonList(httpLoggingInterceptor);
    }

    @Provides
    @Singleton
    @OkHttpNetworkInterceptors
    public List<Interceptor> provideOkHttpNetworkInterceptors(SharedPrefProvider sharedPrefProvider) {
        return singletonList(new BasicAuthInterceptor(sharedPrefProvider));
    }

}
