package zeta.andriod.apps.modules;

import android.content.Context;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import zeta.andriod.apps.environments.FlickrDebugEnv;
import zeta.andriod.apps.sharedPref.DebugSharedPreferences;
import zetaapps.flickr.CachePolicy;
import zetaapps.flickr.Flickr;
import zetaapps.flickr.FlickrConfig;

@Singleton
@Module
public class ConfigModule {

    @Provides
    FlickrConfig provideConfig(Context context, DebugSharedPreferences sharedPreferences) {
        //Debug app uses 10 min's max age cache policy
        final int currentFlickrEnvironment = sharedPreferences.getCurrentFlickrEnvironment();
        final CachePolicy cachePolicy = CachePolicy.create(context.getCacheDir())
                .setCacheMaxAgeInSeconds(TimeUnit.MINUTES.toMinutes(10))
                .build();
        return FlickrConfig.create()
                .setFlickrEnvironment(transform(currentFlickrEnvironment))
                .setCachePolicy(cachePolicy)
                .build();
    }

    @Flickr.Environment
    private int transform(@FlickrDebugEnv int env) {
        switch (env) {
            case FlickrDebugEnv.DEBUG_STAGE:
                return Flickr.Environment.STAGE;
            case FlickrDebugEnv.DEBUG_UAT:
                return Flickr.Environment.UAT;
            default:
            case FlickrDebugEnv.DEBUG_PROD:
                return Flickr.Environment.PROD;
        }
    }

}
