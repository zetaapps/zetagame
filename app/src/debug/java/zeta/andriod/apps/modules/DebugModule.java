package zeta.andriod.apps.modules;

import android.content.Context;

import com.github.pedrovgs.lynx.LynxConfig;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import zeta.andriod.apps.developer.tools.DeveloperToolsImpl;
import zeta.andriod.apps.sharedPref.DebugSharedPreferences;
import zeta.andriod.apps.tools.DeveloperTools;

@Module
@Singleton
public class DebugModule {

    @Provides
    DeveloperTools providesDeveloperTools(DebugSharedPreferences preferences) {
        return new DeveloperToolsImpl(preferences);
    }

    @Provides
    DebugSharedPreferences providesDebugSharedPreferences(Context context) {
        return new DebugSharedPreferences(context);
    }

    @Provides
    public LynxConfig provideLynxConfig() {
        return new LynxConfig();
    }
}
