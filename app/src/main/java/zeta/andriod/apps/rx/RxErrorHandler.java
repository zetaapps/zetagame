package zeta.andriod.apps.rx;

import rx.plugins.RxJavaErrorHandler;
import timber.log.Timber;
import zeta.andriod.apps.BuildConfig;
import zeta.andriod.apps.network.ZetaNoNetworkConnectivityException;

public class RxErrorHandler extends RxJavaErrorHandler {

    @Override
    public void handleError(Throwable e) {
        if (!BuildConfig.DEBUG) {
            return;
        }

        if (e instanceof ZetaNoNetworkConnectivityException) {
            //We don't want to print stack trace here as we know this exception is caused due to
            //no network connectivity
            Timber.e("RxErrorHandler", "No network error");
            return;
        }

        Timber.e("RxErrorHandler", "Uncaught error thrown from Rx: ", e);
    }
}
