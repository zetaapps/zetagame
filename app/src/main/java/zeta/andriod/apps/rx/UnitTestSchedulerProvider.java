package zeta.andriod.apps.rx;

import rx.Scheduler;
import rx.schedulers.Schedulers;
import zeta.andriod.apps.qualifiers.ExistsForTesting;
import zeta.andriod.apps.rx.interfaces.RxSchedulerProvider;

@ExistsForTesting
public class UnitTestSchedulerProvider implements RxSchedulerProvider {
    @Override
    public Scheduler schedulerMainThread() {
        return Schedulers.immediate();
    }

    @Override
    public Scheduler schedulerBackgroundThread() {
        return Schedulers.immediate();
    }
}
