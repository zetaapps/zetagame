package zeta.andriod.apps.rx.interfaces;

import rx.Scheduler;

public interface RxSchedulerProvider {

    Scheduler schedulerMainThread();

    Scheduler schedulerBackgroundThread();
}
