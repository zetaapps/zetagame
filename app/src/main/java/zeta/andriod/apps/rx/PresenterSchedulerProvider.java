package zeta.andriod.apps.rx;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import zeta.andriod.apps.rx.interfaces.RxSchedulerProvider;

public class PresenterSchedulerProvider implements RxSchedulerProvider {
    @Override
    public Scheduler schedulerMainThread() {
        return AndroidSchedulers.mainThread();
    }

    @Override
    public Scheduler schedulerBackgroundThread() {
        return Schedulers.io();
    }
}
