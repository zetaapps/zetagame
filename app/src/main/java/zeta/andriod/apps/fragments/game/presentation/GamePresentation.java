package zeta.andriod.apps.fragments.game.presentation;

import android.support.annotation.StringRes;

import java.util.List;

import zetaapps.flickr.models.flickr.FlickrImages;

public interface GamePresentation {

    void updateGridImages(List<FlickrImages> images, int previousSize);

    void showGridView();

    void hideGridView();

    void flipBackAllImages();

    void flipBackImage(int position);

    void showFooterImage(FlickrImages showFooterImage);

    void showMessage(@StringRes int message);
}
