package zeta.andriod.apps.fragments.home.presenter;

import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;

import javax.annotation.ParametersAreNonnullByDefault;

import zeta.andriod.apps.R;
import zeta.andriod.apps.fragments.common.presenter.RxFragmentLifeCyclePresenter;
import zeta.andriod.apps.fragments.home.presentation.HomePresentation;
import zeta.andriod.apps.rx.ZetaSubscriber;
import zeta.andriod.apps.rx.interfaces.RxSchedulerProvider;
import zetaapps.flickr.managers.params.flickr.FlickrImageManager;
import zetaapps.flickr.models.flickr.FlickrImageModel;
import zetaapps.flickr.models.flickr.errors.FlickrException;

@ParametersAreNonnullByDefault
public class HomePresenter extends RxFragmentLifeCyclePresenter<HomePresentation> {

    private HomePresentation mPresentation;
    private FlickrImageManager mFlickrManager;

    public HomePresenter(RxSchedulerProvider schedulerProvider,
                         FlickrImageManager flickrManager) {
        super(schedulerProvider);
        mFlickrManager = flickrManager;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

    }

    @Override
    public void onCreateView(HomePresentation homePresentation) {
        mPresentation = homePresentation;
        mPresentation.showStartGameButton();
        mPresentation.enableStartGameButton(false);
    }

    @Override
    public void onViewCreated() {
        preFetchTheFlickrResponse();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mPresentation = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void onStartGame() {
        mPresentation.navigateToGamePage();
    }

    private void preFetchTheFlickrResponse() {
        mPresentation.showProgress(true);
        subscribe(mFlickrManager.getFlickrImages(), new ZetaSubscriber<FlickrImageModel, FlickrException>() {
            @Override
            protected void onSuccess(FlickrImageModel success) {
                //We have data show
                mPresentation.requestImageCachingAppWide(success.getFlickrImages());
                mPresentation.showStartGameButton();
                mPresentation.enableStartGameButton(true);
                mPresentation.showProgress(false);
            }

            @Override
            protected void onFailure(@Nullable FlickrException failure) {
                mPresentation.showMessage(R.string.zeta_game_error_loading);
                mPresentation.enableStartGameButton(false);
                mPresentation.showProgress(false);
            }

            @Override
            protected void onNoNetworkFailure() {
                mPresentation.showMessage(R.string.zeta_game_no_network);
                mPresentation.enableStartGameButton(false);
                mPresentation.showProgress(false);
            }
        });
    }

}
