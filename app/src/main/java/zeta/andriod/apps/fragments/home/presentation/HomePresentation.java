package zeta.andriod.apps.fragments.home.presentation;

import android.support.annotation.StringRes;

import java.util.List;

import zetaapps.flickr.models.flickr.FlickrImages;

public interface HomePresentation {

    void showProgress(boolean show);

    void showStartGameButton();

    void enableStartGameButton(boolean enable);

    void showMessage(@StringRes int message);

    void requestImageCachingAppWide(List<FlickrImages> flickrImages);

    void navigateToGamePage();
}
