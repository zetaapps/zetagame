package zeta.andriod.apps.fragments.home.modules;

import dagger.Module;
import dagger.Provides;
import zeta.andriod.apps.dagger.FragmentScope;
import zeta.andriod.apps.fragments.home.presenter.HomePresenter;
import zeta.andriod.apps.rx.interfaces.RxSchedulerProvider;
import zetaapps.flickr.managers.params.flickr.FlickrImageManager;

@Module
@FragmentScope
public class HomePresenterModule {

    @Provides
    HomePresenter providesHomePresenter(RxSchedulerProvider schedulerProvider, FlickrImageManager flickrManager) {
        return new HomePresenter(schedulerProvider, flickrManager);
    }

}
