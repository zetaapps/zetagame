package zeta.andriod.apps.fragments.home;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;

import com.bumptech.glide.Glide;

import java.util.List;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;

import butterknife.Bind;
import zeta.andriod.apps.R;
import zeta.andriod.apps.ZetaAppComponent;
import zeta.andriod.apps.fragments.common.BaseNavigationFragment;
import zeta.andriod.apps.fragments.game.GameFragment;
import zeta.andriod.apps.fragments.home.presentation.HomePresentation;
import zeta.andriod.apps.fragments.home.presenter.HomePresenter;
import zeta.andriod.apps.views.common.BaseViews;
import zeta.andriod.apps.views.utils.ViewUtils;
import zetaapps.flickr.models.flickr.FlickrImages;
import zetaapps.flickr.modules.FlickrImageModule;

@ParametersAreNonnullByDefault
public class HomeFragment extends BaseNavigationFragment implements HomePresentation {

    private Views mViews;
    @Inject
    HomePresenter mPresenter;

    static class Views extends BaseViews {

        @Bind(R.id.start_game_btn)
        Button startGameButton;

        @Bind(R.id.start_progress)
        ProgressBar progressBar;

        Views(View root) {
            super(root);
        }
    }

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Override
    public void configureDependencies(ZetaAppComponent component) {
        component.gameComponent(new FlickrImageModule()).inject(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstance) {
        super.onCreate(savedInstance);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        mViews = new Views(rootView);
        mPresenter.onCreateView(this);
        registerClickListener();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.onViewCreated();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unRegisterClickListener();
        mViews.clear();
        mViews = null;
        mPresenter.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
        mPresenter = null;
    }

    @Override
    public void showProgress(boolean show) {
        ViewUtils.setVisibility(mViews.progressBar, show);
    }

    @Override
    public void showStartGameButton() {
        ViewUtils.setToVisible(mViews.startGameButton);
    }

    @Override
    public void enableStartGameButton(boolean enable) {
        mViews.startGameButton.setEnabled(enable);
    }

    @Override
    public void showMessage(@StringRes int message) {
        Snackbar.make(mViews.getRootView(), getString(message), Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void requestImageCachingAppWide(List<FlickrImages> flickrImages) {
        //Note : We are using getActivity() for a reason. To cache the image to app scope.
        //Glide does the very decent job caching when provided the right context.
        //So that we load the image super quick in the next page.
        for (FlickrImages flickrImage : flickrImages) {
            Glide.with(getActivity()).load(flickrImage.getImageUrl()).preload();
        }
    }

    @Override
    public void navigateToGamePage() {
        if (onSavedInstanceStateCalled()) {
            return;
        }
        getNavigationFragmentManager().addFragmentToBackStack(GameFragment.newInstance());
    }

    private void registerClickListener() {
        mViews.startGameButton.setOnClickListener(view -> mPresenter.onStartGame());
    }

    private void unRegisterClickListener() {
        mViews.startGameButton.setOnClickListener(null);
    }

}
