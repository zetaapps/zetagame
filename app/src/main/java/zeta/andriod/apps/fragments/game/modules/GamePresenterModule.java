package zeta.andriod.apps.fragments.game.modules;

import dagger.Module;
import dagger.Provides;
import zeta.andriod.apps.dagger.FragmentScope;
import zeta.andriod.apps.fragments.game.presenter.GamePresenter;
import zeta.andriod.apps.rx.interfaces.RxSchedulerProvider;
import zetaapps.flickr.managers.params.flickr.FlickrImageManager;

@Module
@FragmentScope
public class GamePresenterModule {

    @Provides
    GamePresenter providesHomePresenter(RxSchedulerProvider schedulerProvider,
                                        FlickrImageManager flickrImageManager) {
        return new GamePresenter(schedulerProvider, flickrImageManager);
    }

}
