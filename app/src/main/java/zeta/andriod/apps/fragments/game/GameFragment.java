package zeta.andriod.apps.fragments.game;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;

import butterknife.Bind;
import zeta.andriod.apps.R;
import zeta.andriod.apps.ZetaAppComponent;
import zeta.andriod.apps.adapters.game.FlickrGameAdapter;
import zeta.andriod.apps.fragments.common.BaseNavigationFragment;
import zeta.andriod.apps.fragments.game.presentation.GamePresentation;
import zeta.andriod.apps.fragments.game.presenter.GamePresenter;
import zeta.andriod.apps.views.common.BaseViews;
import zeta.andriod.apps.views.game.FlipImageView;
import zeta.andriod.apps.views.utils.ViewUtils;
import zetaapps.flickr.models.flickr.FlickrImages;
import zetaapps.flickr.modules.FlickrImageModule;

@ParametersAreNonnullByDefault
public class GameFragment extends BaseNavigationFragment implements GamePresentation {

    //Desired default 3
    private static final int GRID_NUM_COLUMNS = 3;
    private static final int INVALID_GRID_COLUMNS = -1;
    private static final int SINGLE_GRID_COLUMNS = 1;
    private static final int ENTIRE_GRID_COLUMNS = GRID_NUM_COLUMNS;

    private Views mViews;
    private FlickrGameAdapter mFlickrGameAdapter;
    private GridLayoutManager mGridLayoutManager;

    @Inject
    public GamePresenter mPresenter;

    static class Views extends BaseViews {

        @Bind(R.id.zeta_game_grid_recycler_view)
        RecyclerView gridView;

        Views(View root) {
            super(root);
        }
    }

    public static GameFragment newInstance() {
        return new GameFragment();
    }

    @Override
    public void configureDependencies(ZetaAppComponent component) {
        component.gameComponent(new FlickrImageModule()).inject(this);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstance) {
        super.onCreate(savedInstance);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_game, container, false);
        mViews = new Views(rootView);
        initGridView();
        mPresenter.onCreateView(this);
        registerClickListener();
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPresenter.onViewCreated();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unRegisterClickListener();
        mViews.clear();
        mViews = null;
        mPresenter.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.onDestroy();
        mPresenter = null;
        mFlickrGameAdapter = null;
        mGridLayoutManager = null;
    }

    @Override
    public void updateGridImages(List<FlickrImages> images, int previousSize) {
        mFlickrGameAdapter.updateImagesModel(images, previousSize);
    }

    @Override
    public void showGridView() {
        ViewUtils.setToVisible(mViews.gridView);
    }

    @Override
    public void hideGridView() {
        ViewUtils.setToGone(mViews.gridView);
    }

    @Override
    public void flipBackAllImages() {
        mFlickrGameAdapter.flipAllChildViews();
    }

    @Override
    public void flipBackImage(int position) {
        final FlipImageView flipImageView = (FlipImageView) mViews.gridView.getChildAt(position + 1);
        flipImageView.flipBackTheImage();
    }

    @Override
    public void showFooterImage(FlickrImages showFooterImage) {
        mFlickrGameAdapter.showFooterImage(showFooterImage);
    }

    @Override
    public void showMessage(@StringRes int message) {
        Snackbar.make(mViews.getRootView(), getString(message), Snackbar.LENGTH_LONG).show();
    }

    private void registerClickListener() {
        mFlickrGameAdapter.setAdapterClickListener(new FlickrGameAdapter.FlickrGameAdapterListener() {
            @Override
            public void onCountDownFinish() {
                mPresenter.onCountDownFinish();
            }

            @Override
            public void omImageSelected(FlickrImages images, int position) {
                mPresenter.onImageGuessed(images, position);
            }
        });
    }

    private void unRegisterClickListener() {
        mFlickrGameAdapter.setAdapterClickListener(null);
    }

    private void initGridView() {
        final Context context = getContext();
        mFlickrGameAdapter = new FlickrGameAdapter();
        mGridLayoutManager = new GridLayoutManager(context, GRID_NUM_COLUMNS);
        mGridLayoutManager.setSpanSizeLookup(new GridViewSpannedSizeLookUp(mFlickrGameAdapter));
        mViews.gridView.setLayoutManager(mGridLayoutManager);
        mViews.gridView.setHasFixedSize(true);
        mViews.gridView.setAdapter(mFlickrGameAdapter);
    }

    private static class GridViewSpannedSizeLookUp extends GridLayoutManager.SpanSizeLookup {
        private FlickrGameAdapter mGridViewAdapter;

        GridViewSpannedSizeLookUp(FlickrGameAdapter adapter) {
            mGridViewAdapter = adapter;
        }

        @Override
        public int getSpanSize(int position) {
            if (mGridViewAdapter == null) {
                return INVALID_GRID_COLUMNS;
            }
            if (position >= mGridViewAdapter.getItemCount()) {
                return INVALID_GRID_COLUMNS;
            }
            switch (mGridViewAdapter.getItemViewType(position)) {
                case FlickrGameAdapter.HEADER:
                case FlickrGameAdapter.FOOTER:
                    return ENTIRE_GRID_COLUMNS;
                default:
                    return SINGLE_GRID_COLUMNS;
            }
        }
    }
}
