package zeta.andriod.apps.fragments.common.presenter;

import android.support.annotation.CallSuper;

import zeta.andriod.apps.rx.RxSubscriptionManager;
import zeta.andriod.apps.rx.interfaces.RxSchedulerProvider;

public abstract class RxFragmentLifeCyclePresenter<Presentation> extends RxSubscriptionManager implements FragmentLifeCyclePresenter<Presentation> {

    public RxFragmentLifeCyclePresenter(RxSchedulerProvider schedulerProvider) {
        super(schedulerProvider);
    }

    @CallSuper
    @Override
    public void onDestroyView() {
        cancelSubscriptions();
    }

    @Override
    public void onDestroy() {
        // no-op
    }
}
