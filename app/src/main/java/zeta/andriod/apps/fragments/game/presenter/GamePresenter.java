package zeta.andriod.apps.fragments.game.presenter;

import android.support.annotation.Nullable;
import android.view.Menu;
import android.view.MenuInflater;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.annotation.ParametersAreNonnullByDefault;

import zeta.andriod.apps.R;
import zeta.andriod.apps.fragments.common.presenter.RxFragmentLifeCyclePresenter;
import zeta.andriod.apps.fragments.game.presentation.GamePresentation;
import zeta.andriod.apps.rx.ZetaSubscriber;
import zeta.andriod.apps.rx.interfaces.RxSchedulerProvider;
import zetaapps.flickr.managers.params.flickr.FlickrImageManager;
import zetaapps.flickr.models.flickr.FlickrImageModel;
import zetaapps.flickr.models.flickr.FlickrImages;
import zetaapps.flickr.models.flickr.errors.FlickrException;

@ParametersAreNonnullByDefault
public class GamePresenter extends RxFragmentLifeCyclePresenter<GamePresentation> {

    private GamePresentation mPresentation;
    private FlickrImageManager mFlickrManager;
    
    private int mSavedPreviousSize;
    private FlickrImages mSavedShownRandomImage;
    private List<FlickrImages> mSavedFlickrImages = new ArrayList<>(0);
    private List<FlickrImages> mSavedRemainingFlickrImages = new ArrayList<>(0);

    public GamePresenter(RxSchedulerProvider schedulerProvider, FlickrImageManager flickrManager) {
        super(schedulerProvider);
        mFlickrManager = flickrManager;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

    }

    @Override
    public void onCreateView(GamePresentation gamePresentation) {
        mPresentation = gamePresentation;
    }

    @Override
    public void onViewCreated() {
        if (mSavedFlickrImages != null && !mSavedFlickrImages.isEmpty()) {
            mPresentation.updateGridImages(mSavedFlickrImages, mSavedPreviousSize);
            return;
        }
        fetchTheFlickrResponse();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFlickrManager = null;
    }

    public void onCountDownFinish() {
        mSavedShownRandomImage = getRandomImage();
        mPresentation.flipBackAllImages();
        mPresentation.showFooterImage(mSavedShownRandomImage);
    }

    public void onImageGuessed(FlickrImages images, int position) {
        if (mSavedShownRandomImage == null) {
            //Game over!
            mPresentation.showMessage(R.string.zeta_game_over);
            return;
        }

        final String imageUrlFromArray = mSavedShownRandomImage.getImageUrl();
        final String imageUrlFromSelection = images.getImageUrl();
        if (imageUrlFromArray.equalsIgnoreCase(imageUrlFromSelection)) {
            mSavedRemainingFlickrImages.remove(images);
            if (mSavedRemainingFlickrImages == null || mSavedRemainingFlickrImages.isEmpty()) {
                //Game over!
                mPresentation.flipBackImage(position);
                mPresentation.showMessage(R.string.zeta_game_over);
                mSavedShownRandomImage = null;
                return;
            }

            //Next move
            mSavedShownRandomImage = getRandomImage();
            mPresentation.flipBackImage(position);
            mPresentation.showFooterImage(mSavedShownRandomImage);
            mPresentation.showMessage(R.string.zeta_game_yay);
        }
    }

    private void fetchTheFlickrResponse() {
        subscribe(mFlickrManager.getFlickrImages(), new ZetaSubscriber<FlickrImageModel, FlickrException>() {
            @Override
            protected void onSuccess(FlickrImageModel success) {
                final List<FlickrImages> flickrImages = success.getFlickrImages();
                synchronized (this) {
                    //Randomize the order
                    Collections.shuffle(flickrImages);
                    mSavedPreviousSize = flickrImages.size();
                    for (int i = 0; i < flickrImages.size() && i < 9; i++) {
                        final FlickrImages images = flickrImages.get(i);
                        mSavedFlickrImages.add(images);
                    }
                }
                mSavedRemainingFlickrImages.addAll(mSavedFlickrImages);
                mPresentation.updateGridImages(mSavedFlickrImages, mSavedPreviousSize);
                mPresentation.showGridView();
            }

            @Override
            protected void onFailure(@Nullable FlickrException failure) {
                mPresentation.hideGridView();
            }

            @Override
            protected void onNoNetworkFailure() {
                mPresentation.hideGridView();
            }
        });
    }

    private FlickrImages getRandomImage() {
        final int size = mSavedRemainingFlickrImages.size();
        if (size == 1) {
            return mSavedRemainingFlickrImages.get(0);
        }
        Random randomGenerator = new Random();
        return mSavedRemainingFlickrImages.get(randomGenerator.nextInt(size));
    }

}
