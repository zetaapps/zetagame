package zeta.andriod.apps.fragments.subcomponents;

import dagger.Subcomponent;
import zeta.andriod.apps.dagger.FragmentScope;
import zeta.andriod.apps.fragments.game.GameFragment;
import zeta.andriod.apps.fragments.game.modules.GamePresenterModule;
import zeta.andriod.apps.fragments.home.HomeFragment;
import zeta.andriod.apps.fragments.home.modules.HomePresenterModule;
import zetaapps.flickr.modules.FlickrImageModule;

@FragmentScope
@Subcomponent(modules = {HomePresenterModule.class, GamePresenterModule.class, FlickrImageModule.class})
public interface GameComponent {

    void inject(HomeFragment fragment);

    void inject(GameFragment fragment);

}
