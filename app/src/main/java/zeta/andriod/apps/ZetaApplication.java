package zeta.andriod.apps;

import android.content.Context;
import android.support.multidex.MultiDexApplication;

import org.greenrobot.eventbus.EventBus;

import javax.annotation.ParametersAreNonnullByDefault;
import javax.inject.Inject;

import dagger.Lazy;
import rx.plugins.RxJavaPlugins;
import zeta.andriod.apps.modules.ZetaAppModule;
import zeta.andriod.apps.rx.RxErrorHandler;
import zeta.andriod.apps.tools.DeveloperTools;

@ParametersAreNonnullByDefault
public class ZetaApplication extends MultiDexApplication {

    @Inject
    public Lazy<EventBus> mEventBus;

    @Inject
    public Lazy<DeveloperTools> mDeveloperTools;

    private ZetaAppComponent mZetaAppComponent;

    public static void watchForMemoryLeaks(Context context, Object object) {
        ZetaApplication application = (ZetaApplication) context.getApplicationContext();
        application.mDeveloperTools.get().watchForMemoryLeaks(object);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        final Context context = getApplicationContext();
        mZetaAppComponent = DaggerZetaAppComponent.builder()
                .zetaAppModule(new ZetaAppModule(this, context))
                .build();
        mZetaAppComponent.inject(this);
        mDeveloperTools.get().initialize(this);

        //Just to log the Rx Global errors.
        RxJavaPlugins.getInstance().registerErrorHandler(new RxErrorHandler());
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        //Do cleans ups
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        //Do cleans ups
    }

    //region Dependency Injection
    public ZetaAppComponent getZetaAppComponent() {
        return mZetaAppComponent;
    }
    //endregion
}
