package zeta.andriod.apps.providers.interfaces;

public interface ConnectivityProvider {
    boolean isConnected();

    boolean isWifiNetwork();
}
