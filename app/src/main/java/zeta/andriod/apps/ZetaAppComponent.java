package zeta.andriod.apps;

import javax.inject.Singleton;

import dagger.Component;
import zeta.andriod.apps.component.NavigationActivityComponent;
import zeta.andriod.apps.developer.debug.subcomponents.DebugComponent;
import zeta.andriod.apps.fragments.subcomponents.GameComponent;
import zeta.andriod.apps.modules.ConfigModule;
import zeta.andriod.apps.modules.DebugModule;
import zeta.andriod.apps.modules.EventBusModule;
import zeta.andriod.apps.modules.EventBusNoSubscriberModule;
import zeta.andriod.apps.modules.NetworkModule;
import zeta.andriod.apps.modules.OkHttpInterceptorsModule;
import zeta.andriod.apps.modules.ZetaAppModule;
import zetaapps.flickr.FlickrModule;
import zetaapps.flickr.modules.FlickrImageModule;

@Singleton
@Component(modules = {
        DebugModule.class,
        NetworkModule.class,
        EventBusModule.class,
        ZetaAppModule.class,
        ConfigModule.class,
        FlickrModule.class,
        OkHttpInterceptorsModule.class,
        EventBusNoSubscriberModule.class})
public interface ZetaAppComponent {

    NavigationActivityComponent navigationActivity();

    GameComponent gameComponent(FlickrImageModule flickrImageModule);

    DebugComponent debugComponent();

    void inject(ZetaApplication targetApplication);

}
