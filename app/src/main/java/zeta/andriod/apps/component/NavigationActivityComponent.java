package zeta.andriod.apps.component;

import dagger.Subcomponent;
import zeta.andriod.apps.activities.NavigationActivity;
import zeta.andriod.apps.dagger.ActivityScope;

@ActivityScope
@Subcomponent
public interface NavigationActivityComponent {

    void inject(NavigationActivity activity);

}