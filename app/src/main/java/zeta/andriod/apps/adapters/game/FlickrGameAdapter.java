package zeta.andriod.apps.adapters.game;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.annotation.ParametersAreNonnullByDefault;

import zeta.andriod.apps.R;
import zeta.andriod.apps.adapters.common.BaseHeaderAndFooterAdapter;
import zeta.andriod.apps.views.game.FlipImageView;
import zeta.andriod.apps.views.game.FooterView;
import zeta.andriod.apps.views.game.HeaderView;
import zetaapps.flickr.models.flickr.FlickrImages;

@ParametersAreNonnullByDefault
public class FlickrGameAdapter extends BaseHeaderAndFooterAdapter {

    private List<FlickrImages> mImages;
    private FlickrImages mFooterImage;
    private boolean mIsAllImageFlipped = false;
    private FlickrGameAdapterListener mListener;
    private WeakReference<FooterView> mFooterViewReference;

    public interface FlickrGameAdapterListener {
        void onCountDownFinish();

        void omImageSelected(FlickrImages images, int position);
    }

    public FlickrGameAdapter() {
        mImages = new ArrayList<>(0);
    }

    public void setAdapterClickListener(@Nullable FlickrGameAdapterListener listener) {
        mListener = listener;
    }

    public void updateImagesModel(List<FlickrImages> imagesList,
                                  int previousSize) {
        mImages = imagesList;
        if (previousSize == 0) {
            notifyDataSetChanged();
        } else {
            final int positionStart = previousSize + 1;
            notifyItemRangeInserted(positionStart, mImages.size());
        }
    }

    public void showFooterImage(FlickrImages flickrImages) {
        if (mFooterImage == null) {
            mFooterImage = flickrImages;
            notifyItemChanged(getItemCount());
            return;
        }

        mFooterImage = flickrImages;
        if (mFooterViewReference == null || mFooterViewReference.get() == null) {
            return;
        }
        mFooterViewReference.get().refreshFooterImage(mFooterImage.getImageUrl());
    }

    public void flipAllChildViews() {
        mIsAllImageFlipped = true;
        notifyItemRangeChanged((canShowHeader() ? 1 : 0), getRegularItemCount());
    }

    @Override
    protected long getStickyHeaderIdentifier(int position) {
        return 0;
    }

    @Override
    protected View onCreateStickyHeaderViewHolder(ViewGroup viewGroup) {
        return null;
    }

    @Override
    protected void onBindStickyHeaderViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        //no op
    }

    @Override
    protected boolean canShowHeader() {
        return true;
    }

    @Override
    protected boolean canShowFooter() {
        return mFooterImage != null;
    }

    @Override
    protected int getRegularItemViewType(int position) {
        return 0;
    }

    @Override
    protected int getRegularItemCount() {
        return mImages.size();
    }

    @Override
    protected View onCreateRegularItemHeaderViewHolder(ViewGroup parent) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return inflater.inflate(R.layout.view_grid_header_inflatable, parent, false);
    }

    @Override
    protected View onCreateRegularItemFooterViewHolder(ViewGroup parent) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final FooterView footerView = ((FooterView) inflater.inflate(R.layout.view_grid_footer_inflatable, parent, false));
        mFooterViewReference = new WeakReference<>(footerView);
        return footerView;
    }

    @Override
    protected View onCreateRegularViewHolder(ViewGroup parent, int viewType) {
        final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return inflater.inflate(R.layout.view_grid_item_inflatable, parent, false);
    }

    @Override
    protected void onBindRegularItemHeaderViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final HeaderView headerView = ((HeaderView) viewHolder.itemView);
        headerView.setOnClickListener(() -> {
            if (mListener == null) {
                return;
            }
            mListener.onCountDownFinish();
        });
        headerView.setHeaderData(TimeUnit.SECONDS.toMillis(15));
    }

    @Override
    protected void onBindRegularItemFooterViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final FooterView footerView = ((FooterView) viewHolder.itemView);
        if (mFooterImage == null) {
            return;
        }
        footerView.setFooterData(mFooterImage.getImageUrl());
    }

    @Override
    protected void onBindRegularViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        final FlickrImages images = mImages.get(position);
        final FlipImageView imageView = ((FlipImageView) viewHolder.itemView);
        imageView.setOnClickListener(view -> {
            if (mListener == null) {
                return;
            }
            mListener.omImageSelected(images, position);
        });
        imageView.setImageData(images.getImageUrl(), mIsAllImageFlipped);
    }
}
