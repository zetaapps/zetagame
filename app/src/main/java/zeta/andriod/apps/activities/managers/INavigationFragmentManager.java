package zeta.andriod.apps.activities.managers;

public interface INavigationFragmentManager {

    NavigationFragmentManager getNavigationFragmentManager();
}
