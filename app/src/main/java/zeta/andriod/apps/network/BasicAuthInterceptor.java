package zeta.andriod.apps.network;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import zeta.andriod.apps.providers.interfaces.SharedPrefProvider;
import zeta.andriod.apps.qualifiers.Hack;
import zeta.andriod.apps.qualifiers.HackType;

@Hack(explanation = "Basic auth for the demo purpose only", hackType = HackType.FOR_DEMO_ONLY)
public class BasicAuthInterceptor implements Interceptor {

    private SharedPrefProvider sharedPrefProvider;

    public BasicAuthInterceptor(SharedPrefProvider sharedPrefProvider) {
        this.sharedPrefProvider = sharedPrefProvider;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        final String baseAuth = sharedPrefProvider.getBaseAuth();
        if (baseAuth == null) {
            return chain.proceed(original);
        }

        //Add basic auth if available
        Request.Builder requestBuilder = original.newBuilder()
                .header("Authorization", baseAuth)
                .method(original.method(), original.body());

        Request request = requestBuilder.build();
        return chain.proceed(request);
    }
}
