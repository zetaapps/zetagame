package zeta.andriod.apps.views.game;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import butterknife.Bind;
import zeta.andriod.apps.R;
import zeta.andriod.apps.views.common.BaseViews;

public class HeaderView extends FrameLayout {

    private Views mViews;
    private HeaderListener mListener;

    public interface HeaderListener {
        void onCountDownFinish();
    }

    static class Views extends BaseViews {
        @Bind(R.id.header_title_text_view)
        TextView title;

        @Bind(R.id.count_down_timer_text_view)
        CountDownTimerTextView countDownTimer;

        Views(View root) {
            super(root);
        }
    }

    public HeaderView(Context context) {
        super(context);
        init();
    }

    public HeaderView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HeaderView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setOnClickListener(HeaderListener listener) {
        mListener = listener;
    }

    public void setHeaderData(long milliSeconds) {
        mViews.countDownTimer.setTime(milliSeconds);
        mViews.countDownTimer.setPrefixText("Timer");
        mViews.countDownTimer.setSuffixText("!");
        mViews.countDownTimer.setOnTimerListener(new CountDownTimerTextView.TimerListener() {
            @Override
            public void onTick(long millisUntilFinished) {
                //No op
            }

            @Override
            public void onFinish() {
                if (mListener == null) {
                    return;
                }
                mListener.onCountDownFinish();
            }
        });
        mViews.countDownTimer.startCountDown();
    }

    private void init() {
        inflate(getContext(), R.layout.view_header, this);
        mViews = new Views(this);
    }

}
