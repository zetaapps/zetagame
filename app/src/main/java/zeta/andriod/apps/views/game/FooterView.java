package zeta.andriod.apps.views.game;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import butterknife.Bind;
import zeta.andriod.apps.R;
import zeta.andriod.apps.appconfig.GlideConfigModule;
import zeta.andriod.apps.views.common.BaseViews;
import zeta.andriod.apps.views.utils.ViewUtils;

public class FooterView extends FrameLayout {

    private Views mViews;
    private FooterListener mListener;

    public interface FooterListener {
        void onCountDownFinish();
    }

    static class Views extends BaseViews {
        @Bind(R.id.footer_image_view)
        ImageView imageView;

        @Bind(R.id.footer_title)
        TextView footerTitle;

        Views(View root) {
            super(root);
        }
    }

    public FooterView(Context context) {
        super(context);
        init();
    }

    public FooterView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FooterView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void setOnClickListener(FooterListener listener) {
        mListener = listener;
    }

    public void setFooterData(@Nullable String imageUrl) {
        if (imageUrl == null) {
            return;
        }
        Glide.with(getContext())
                .load(imageUrl)
                .into(mViews.imageView);
        ViewUtils.setMultipleToVisible(mViews.imageView, mViews.footerTitle);
    }

    public void refreshFooterImage(@Nullable String imageUrl) {
        if (imageUrl == null) {
            return;
        }
        Glide.clear(mViews.imageView);
        Glide.with(getContext())
                .load(imageUrl)
                .dontAnimate()
                .thumbnail(GlideConfigModule.SIZE_MULTIPLIER)
                .into(mViews.imageView);
    }

    private void init() {
        inflate(getContext(), R.layout.view_footer, this);
        mViews = new Views(this);
    }
}
