package zeta.andriod.apps.views.game;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import com.bumptech.glide.Glide;

import butterknife.Bind;
import zeta.andriod.apps.R;
import zeta.andriod.apps.views.animations.AnimationFactory;
import zeta.andriod.apps.views.common.BaseViews;

public class FlipImageView extends FrameLayout {

    private Views mViews;

    static class Views extends BaseViews {
        @Bind(R.id.zeta_view_flipper)
        ViewFlipper viewFlipper;

        @Bind(R.id.zeta_view_image_1)
        ImageView imageView1;

        @Bind(R.id.zeta_view_image_2)
        ImageView imageView2;

        Views(View root) {
            super(root);
        }
    }

    public FlipImageView(Context context) {
        super(context);
        init();
    }

    public FlipImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FlipImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void flipBackTheImage() {
        mViews.viewFlipper.setClickable(false);
        mViews.imageView1.setClickable(false);
        mViews.imageView2.setClickable(false);
        mViews.viewFlipper.showNext();
    }

    public void setImageData(String imageUrl, boolean isImageFlipped) {
        //Note : Here we are using getContext() per view. As a indication to glide that it should do micro
        //caching for this view. Anyways we have requested preload else for these images for app scope.
        //Image load should be super fast.
        Glide.with(getContext())
                .load(imageUrl)
                .into(mViews.imageView1);

        Glide.with(getContext())
                .load(R.drawable.question)
                .into(mViews.imageView2);

        if (isImageFlipped) {
            AnimationFactory.flipTransition(mViews.viewFlipper, AnimationFactory.FlipDirection.RIGHT_LEFT);
        }
        mViews.viewFlipper.setClickable(!isImageFlipped);
        mViews.imageView1.setClickable(!isImageFlipped);
        mViews.imageView2.setClickable(!isImageFlipped);
    }

    private void init() {
        inflate(getContext(), R.layout.view_image_flipper, this);
        mViews = new Views(this);
        mViews.viewFlipper.setOnClickListener(view -> AnimationFactory.flipTransition(mViews.viewFlipper, AnimationFactory.FlipDirection.LEFT_RIGHT));
        mViews.imageView1.setOnClickListener(view -> AnimationFactory.flipTransition(mViews.viewFlipper, AnimationFactory.FlipDirection.LEFT_RIGHT));
        mViews.imageView2.setOnClickListener(view -> AnimationFactory.flipTransition(mViews.viewFlipper, AnimationFactory.FlipDirection.LEFT_RIGHT));
    }

}
