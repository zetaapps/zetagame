package zeta.andriod.apps.dagger;

import javax.inject.Scope;

@Scope
public @interface ActivityScope {
}
