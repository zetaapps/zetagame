package zeta.andriod.apps.modules;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import zeta.andriod.apps.providers.DefaultConnectivityProvider;
import zeta.andriod.apps.providers.DefaultSharedPrefProvider;
import zeta.andriod.apps.providers.DefaultStringResourceProvider;
import zeta.andriod.apps.providers.interfaces.ConnectivityProvider;
import zeta.andriod.apps.providers.interfaces.SharedPrefProvider;
import zeta.andriod.apps.providers.interfaces.StringResourceProvider;
import zeta.andriod.apps.rx.PresenterSchedulerProvider;
import zeta.andriod.apps.rx.RxSubscriptionManager;
import zeta.andriod.apps.rx.interfaces.RxSchedulerProvider;

@Module
@Singleton
public class ZetaAppModule {

    private final Context appContext;
    private final Application application;

    public ZetaAppModule(Application application, Context appContext) {
        this.appContext = appContext;
        this.application = application;
    }

    @Provides
    Application providesApplication() {
        return application;
    }

    @Provides
    Context providesAppContext() {
        return appContext;
    }

    @Provides
    StringResourceProvider providesStringResourceProvider(Context context) {
        return new DefaultStringResourceProvider(context);
    }

    @Provides
    SharedPrefProvider providesSharedPrefProvider(Context context) {
        return new DefaultSharedPrefProvider(context);
    }

    @Provides
    ConnectivityProvider providesConnectivityProvider(Context context) {
        return new DefaultConnectivityProvider(context);
    }

    @Provides
    RxSubscriptionManager providesRxSubscriptionManager(RxSchedulerProvider rxSchedulerProvider) {
        return new RxSubscriptionManager(rxSchedulerProvider);
    }

    @Provides
    RxSchedulerProvider providesRxSchedulerProvider() {
        return new PresenterSchedulerProvider();
    }

}
