package zeta.andriod.apps.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import zeta.andriod.apps.eventBus.DebugNoEventSubscribersHandler;
import zeta.andriod.apps.eventBus.NoEventSubscribersHandler;

@Module
@Singleton
public class EventBusNoSubscriberModule {

    @Provides
    NoEventSubscribersHandler providerNoEventSubscribersHandler() {
        return new DebugNoEventSubscribersHandler();
    }

}
