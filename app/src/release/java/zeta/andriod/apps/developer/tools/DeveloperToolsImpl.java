package zeta.andriod.apps.developer.tools;

import javax.annotation.ParametersAreNonnullByDefault;

import zeta.andriod.apps.ZetaApplication;
import zeta.andriod.apps.sharedPref.DebugSharedPreferences;
import zeta.andriod.apps.tools.DeveloperTools;

@ParametersAreNonnullByDefault
public class DeveloperToolsImpl implements DeveloperTools {

    public DeveloperToolsImpl(DebugSharedPreferences preferences) {
        //no ops
    }

    @Override
    public void initialize(ZetaApplication application) {
        //No ops
    }

    @Override
    public void watchForMemoryLeaks(Object object) {
        //No ops
    }
}
