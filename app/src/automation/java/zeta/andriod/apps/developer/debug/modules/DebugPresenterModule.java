package zeta.andriod.apps.developer.debug.modules;

import dagger.Module;
import dagger.Provides;
import zeta.andriod.apps.dagger.FragmentScope;
import zeta.andriod.apps.developer.debug.presenter.DebugPresenter;
import zeta.andriod.apps.rx.interfaces.RxSchedulerProvider;

@Module
@FragmentScope
public class DebugPresenterModule {

    @Provides
    DebugPresenter providesHomePresenter(RxSchedulerProvider schedulerProvider) {
        return new DebugPresenter(schedulerProvider);
    }

}
