package zeta.andriod.apps.developer.debug.subcomponents;

import dagger.Subcomponent;
import zeta.andriod.apps.dagger.FragmentScope;
import zeta.andriod.apps.developer.debug.DebugFragment;
import zeta.andriod.apps.developer.debug.modules.DebugPresenterModule;

@FragmentScope
@Subcomponent(modules = {DebugPresenterModule.class})
public interface DebugComponent {

    void inject(DebugFragment fragment);
}
