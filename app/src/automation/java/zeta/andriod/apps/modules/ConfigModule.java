package zeta.andriod.apps.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import zetaapps.flickr.FlickrConfig;

@Singleton
@Module
public class ConfigModule {

    @Provides
    FlickrConfig provideConfig() {
        //Release uses default configurations
        return FlickrConfig.create().build();
    }
}
